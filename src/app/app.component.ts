import { Component } from '@angular/core';
import { Post } from './post';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front-qualibri';

  post:Post[]
  constructor(
    private dataService: DataService
  ) {
    
  }
}
