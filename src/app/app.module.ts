import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { MaterialModule } from './shared/material/material.module'

import { HomePageComponent } from './pages/homepage/homepage.component';
import { SearchBarComponent } from './pages/searchbar/searchbar.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { RulesComponent } from './pages/rules/rules.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { DataService } from './data.service';

import { LoginComponent } from './core/authentification/login/login.component'

import { DocumentsListComponent } from './features/documents/components/documents-list/documents-list.component';
import { DocumentFormComponent } from './features/documents/components/document-form/document-form.component';
import { DocumentDetailsComponent } from './features/documents/components/document-details/document-details.component'
import { SidenavComponent } from './core/ui/sidenav/sidenav.component'
import { NotFoundComponent } from './core/ui/not-found/not-found.component'
import { HeaderComponent } from './core/ui/header/header.component'
import { FooterComponent } from './core/ui/footer/footer.component'
import { GabaritComponent } from './core/ui/gabarit/gabarit.component'
import { UsersModule } from './features/users/users.module'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DocumentSearchRuleComponent } from './features/components/document-search-rule/document-search-rule.component'

@NgModule({
    declarations: [
        // Root Features
        AppComponent,
        SidenavComponent,
        NotFoundComponent,
        HeaderComponent,
        FooterComponent,
        GabaritComponent,

        // Users Features
        LoginComponent,
        
        // Workflow Features
        HomePageComponent,
        SearchBarComponent,
        SettingsComponent,
        RulesComponent,
        DocumentSearchRuleComponent,

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        UsersModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: () => {
                    if (localStorage.getItem('auth')) {
                        var auth = JSON.parse(localStorage.getItem('auth'))
                        return auth.token
                    }
                    return null
                }
            }
        })
    ],
    providers: [ DataService, JwtHelperService ],
    bootstrap: [AppComponent]
})
export class AppModule {}
