import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { JwtHelperService } from '@auth0/angular-jwt'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private http: HttpClient, public jwtHelper: JwtHelperService) {}

    getUsers(auth): Promise<any> {
        const url = '/users/getUsers'
        return this.http
            .get(url, { headers: auth })
            .toPromise()
            .then(this.treating)
            .catch(this.treatingBad)
    }

    postLogin(data): Promise<any> {
        const url = '/api/users/login'
        console.log('data', data)

        return this.http
            .post(url, data)
            .toPromise()
            .then(this.treating)
            .catch(this.treatingBad)
    }

    // headerAuth(){
    //   if(localStorage.getItem('auth')){
    //     var auth = JSON.parse(localStorage.getItem('auth'));
    //   }
    //   const headers = new Headers({
    //     'Content-Type': 'application/json',
    //     'Authorization': auth.token
    //   })
    //   return headers
    // }

    isAuthenticated(): boolean {
        if (localStorage.getItem('auth')) {
            var auth = JSON.parse(localStorage.getItem('auth'))
            return !this.jwtHelper.isTokenExpired(auth.token)
        }
    }

    getMe(): Promise<any> {
        const userFake = Promise.resolve({
            id: 5,
            user_name: 'Marinette',
            user_status: null,
            user_is_displayed: null,
            user_email: null,
            user_password: '$2a$08$cI7vrqLsrQDbcSZIIiZTaedVcf3ZauX5UbJIR2cshTixhr9BvVlaS',
            user_token:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjUsImlhdCI6MTU3ODU4OTg0OX0.bfnBqSigxveFAowqSGquXlOKSevHdcL2Bo82zsyoCmQ',
            createdAt: '2020-01-09T17:10:49.000Z',
            updatedAt: '2020-01-09T17:10:49.000Z'
        })
        return userFake
    }

    private treating(res: any) {
        let body = res
        return body
    }

    /**
     * la Methode retourne le message d'erreur ou l'erreur même
     *
     * @private
     * @param {*} error
     * @returns {Promise<any>}
     * @memberof ConsultantsService
     */
    private treatingBad(error: any): Promise<any> {
        console.error('An error occurred', error) // for development purposes only
        return Promise.reject(error.message || error)
    }
}
