import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  usernameCtrl: FormControl;
  passwordCtrl: FormControl;
  

  constructor(fb: FormBuilder, private userService: AuthService, private router: Router) {
    this.usernameCtrl = fb.control('', [Validators.required, Validators.minLength(3)]);
    this.passwordCtrl = fb.control('', Validators.required);
    
    this.userForm = fb.group({
      username: this.usernameCtrl,
      password: this.passwordCtrl
    });
  }

  ngOnInit() {
  }

  register() {
    
    // version prod
    this.userService.postLogin(
      {
      "user_name": this.usernameCtrl.value ,
      "user_password": this.passwordCtrl.value
    }).then( res => {
      localStorage.setItem('auth', JSON.stringify(res));
      this.router.navigate(['/documents/list']);
    });
  }


    // version dev
    // this.userService.postLogin(
    //   {
    //   "user_name": "Marinette1" ,
    //   "user_password": "azerty1!"
    // }).then( res => {
    //   console.log("res " , res);
      
      
    //   localStorage.setItem('auth', JSON.stringify(res));
    //   this.router.navigate(['/rules']);
    // });
  //}

  
}
