import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'app-gabarit',
    templateUrl: './gabarit.component.html',
    styleUrls: ['./gabarit.component.scss']
})
export class GabaritComponent implements OnInit {
    isExpanded = false

    constructor() {}

    ngOnInit() {}

    handleExpand() {
        console.log('coucou')
        this.isExpanded = !this.isExpanded
    }
}
