import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { ViewEncapsulation } from '@angular/core'

@Component({
    selector: 'app-sidenav',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
    hidden = true

    @Input() isExpanded: boolean
    @Output() expand: EventEmitter<void> = new EventEmitter()

    constructor() {}

    handleExpandClick() {
        this.expand.emit()
        this.hidden = !this.hidden
    }

    ngOnInit() {}
}
