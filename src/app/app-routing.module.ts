import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from './core/authentification/login/login.component'
import { AuthGuardService as AuthGuard } from './core/authentification/auth-guard.service'


import { SettingsComponent } from './pages/settings/settings.component'
import { RulesComponent } from './pages/rules/rules.component'
import { NotFoundComponent } from './core/ui/not-found/not-found.component'


const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'documents', loadChildren: () => import('./features/documents/documents.module').then(mod => mod.DocumentsModule), canActivate: [
        AuthGuard
    ]
 },
    { path: 'users', loadChildren: () => import('./features/users/users.module').then(mod => mod.UsersModule) },
   
    // rules a supprimer
    // { path: 'rules', component: RulesComponent,  canActivate: [
    //     AuthGuard
    // ] },

    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: '**', component: NotFoundComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
