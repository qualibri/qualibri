import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(): Promise<any> {
    const url = 'api/users/getUsers';
    return this.http.get(url).toPromise().catch(err => {
      console.error('An error occurred', err);
    })
  }

  getUserById(id): Promise<any> {
    const url = "api/users/" + id;
    return this.http.get(url).toPromise().catch(err => {
      console.error('An error occurred', err);
    })
  }

  addUser(data): Promise<any> {
    const url = "api/users/create";
    return this.http.post(url, data).toPromise().catch(err => {
      console.error('An error occurred', err);
    })
  }

  updateUser(id, data): Promise<any> {
    const url = "api/users/" + id;
    return this.http.put(url, data).toPromise().catch(err => {
      console.error('An error occurred', err);
    })
  }

  deleteUser(id): Promise<any> {
    const url = "api/users/delete/" + id;
    return this.http.put(url, 'void').toPromise().catch(err => {
      console.error('An error occurred', err);
    })
  }

}
