import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../shared/users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  user;

  constructor(private route: ActivatedRoute, private userservice: UsersService, private router: Router) { }

  ngOnInit() {
    this.getUser() 
  }

  deleteUser(){
    this.userservice.deleteUser(this.user.id)
    this.router.navigate(['/users/list'])
    console.log("User was deleted..");
  }
  

  async getUser(){
    let user_id;
    this.route.params.subscribe(params => {
      user_id = parseInt(params.id);
     });
    this.user = await this.userservice.getUserById(user_id)
  }
}
