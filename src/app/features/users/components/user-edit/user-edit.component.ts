import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../shared/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  user

  userForm: FormGroup;
  usernameCtrl: FormControl;
  roleCtrl: FormControl;

  constructor(fb: FormBuilder, private userService: UsersService, private router: Router, private route: ActivatedRoute) {
    this.usernameCtrl = fb.control('');
    this.roleCtrl = fb.control('', Validators.required);
    
    this.userForm = fb.group({
      username: this.usernameCtrl,
      role: this.roleCtrl
    });
  }

  ngOnInit() {
    this.getUser() 
  }

  async getUser(){
    let user_id;
    this.route.params.subscribe(params => {
      user_id = parseInt(params.id);
     });
    this.user = await this.userService.getUserById(user_id)
  }

  async register() {
    let userLoad = this.usernameCtrl.value
    if(userLoad == ''){
      userLoad = this.user.user_name
    }
    let data = {
      "user_name": userLoad, 
      "user_role": this.roleCtrl.value
    }
    await this.userService.updateUser(this.user.id, data)
    this.router.navigate(['/users/list']);
  }
}
