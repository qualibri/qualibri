import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../shared/users.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})


export class UsersListComponent implements OnInit {
  users;


  displayedColumns: string[] = ['name', 'email', 'role' ,'id'];
  dataSource = new MatTableDataSource();

  constructor(private usersservice: UsersService) { 
  }

  ngOnInit() {
    this.buildTable();
  }

  async buildTable() {
    try {
      this.users = await this.usersservice.getUsers();
      this.dataSource = new MatTableDataSource(this.users);
    } catch (err) {
      console.error('Error: ${err.Message}');
    }
  }
 

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
