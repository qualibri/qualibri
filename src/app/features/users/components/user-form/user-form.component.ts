import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../shared/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  userForm: FormGroup;
  usernameCtrl: FormControl;
  passwordCtrl: FormControl;
  roleCtrl: FormControl;
  

  constructor(fb: FormBuilder, private userService: UsersService, private router: Router) {
    this.usernameCtrl = fb.control('', [Validators.required, Validators.minLength(3)]);
    this.passwordCtrl = fb.control('', Validators.required);
    this.roleCtrl = fb.control('', Validators.required);
    
    this.userForm = fb.group({
      username: this.usernameCtrl,
      password: this.passwordCtrl,
      role: this.roleCtrl
    });
  }

  ngOnInit() {
  }

  async register() {
    await this.userService.addUser({
        "user_name": this.usernameCtrl.value,
        "user_password": this.passwordCtrl.value,
        "user_role": this.roleCtrl.value
      })
    this.router.navigate(['/users/list']);
  }

}
