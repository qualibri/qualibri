import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentSearchRuleComponent } from './document-search-rule.component';

describe('DocumentSearchRuleComponent', () => {
  let component: DocumentSearchRuleComponent;
  let fixture: ComponentFixture<DocumentSearchRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentSearchRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentSearchRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
