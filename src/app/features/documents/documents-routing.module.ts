import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentDetailsComponent } from './components/document-details/document-details.component';
import { DocumentsModule } from './documents.module';
import { DocumentsListComponent } from './components/documents-list/documents-list.component';
import { DocumentFormComponent } from './components/document-form/document-form.component';
import { DocumentSearchRuleComponent } from './components/document-search-rule/document-search-rule.component';


const routes: Routes = [
  { path: "list", component: DocumentsListComponent },
  { path: "create", component: DocumentFormComponent },
  { path: "search", component: DocumentSearchRuleComponent},
  { path: ":id", component: DocumentDetailsComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsRoutingModule { }
