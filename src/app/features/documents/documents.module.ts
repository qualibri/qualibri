import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentsRoutingModule } from './documents-routing.module';
import { DocumentDetailsComponent } from './components/document-details/document-details.component';
import { DocumentsListComponent } from './components/documents-list/documents-list.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { DocumentFormComponent } from './components/document-form/document-form.component';
import { DocumentSearchRuleComponent } from './components/document-search-rule/document-search-rule.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [DocumentDetailsComponent, DocumentsListComponent, DocumentFormComponent, DocumentSearchRuleComponent],
  imports: [
    CommonModule,
    DocumentsRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class DocumentsModule { }
