import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DocumentsService } from '../../shared/documents.service';

@Component({
  selector: 'app-document-details',
  templateUrl: './document-details.component.html',
  styleUrls: ['./document-details.component.scss']
})
export class DocumentDetailsComponent implements OnInit {
  documentMore // info des regles 
  document = [''] // info sur le document

  constructor( private route: ActivatedRoute, private documentservice: DocumentsService) { }

  ngOnInit(): void {
  this.getDocumentMore()
  this.getDocument()  }

  async getDocumentMore(){
    let doc_id;
    this.route.params.subscribe(params => {
      doc_id = parseInt(params.id);
     });
    this.documentMore = await this.documentservice.getDocumentRulesByDocId(doc_id)
  }

  async getDocument(){
    let doc_id;
    this.route.params.subscribe(params => {
      doc_id = parseInt(params.id);
     });
    this.document = await this.documentservice.getDocumentById(doc_id)
  }
}
