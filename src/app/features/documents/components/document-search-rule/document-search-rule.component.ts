import { Component, OnInit } from '@angular/core';
import { DocumentsService } from '../../shared/documents.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-document-search-rule',
  templateUrl: './document-search-rule.component.html',
  styleUrls: ['./document-search-rule.component.scss']
})
export class DocumentSearchRuleComponent implements OnInit {

  documentValues
  input = new FormControl();

  constructor(private documentService: DocumentsService) { }

  ngOnInit() {
    this.input.valueChanges.subscribe(value => {
      this.documentService.searchInDocument(value).then(results => this.documentValues = results);
    });
  }


}
