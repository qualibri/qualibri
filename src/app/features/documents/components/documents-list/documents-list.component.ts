import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { DocumentsService } from '../../shared/documents.service';

@Component({
  selector: 'app-documents-list',
  templateUrl: './documents-list.component.html',
  styleUrls: ['./documents-list.component.scss']
})
export class DocumentsListComponent implements OnInit {
  documents;

  displayedColumns: string[] = ['name', 'email', 'role' ,'id'];
  dataSource = new MatTableDataSource();

  constructor(private documentservice: DocumentsService) {
  }

 ngOnInit() {
    this.buildTable();
  }

  async buildTable() {
    try {
      this.documents = await this.documentservice.getDocuments();
      this.dataSource = new MatTableDataSource(this.documents);
    } catch (err) {
      console.error('Error: ${err.Message}');
    }
  }
 

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
