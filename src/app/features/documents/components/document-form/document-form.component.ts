import { Component, OnInit } from '@angular/core';
import { DocumentsService } from '../../shared/documents.service';

@Component({
  selector: 'app-document-form',
  templateUrl: './document-form.component.html',
  styleUrls: ['./document-form.component.scss']
})
export class DocumentFormComponent implements OnInit {

  fileToUpload: File = null;

  constructor(private documentService: DocumentsService) { }

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  uploadFileToActivity() {
    this.documentService.postFile(this.fileToUpload).then( res => console.log("doc is sent"));
  }

}
