import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  constructor(private http: HttpClient) { }

  getDocuments(): Promise<any> {
    const url = 'api/document';
    return this.http.get(url).toPromise().catch(err => {
      console.error('GET [Documents] An error occurred', err);
    })
  }

  getDocumentById(id): Promise<any> {
    const url = 'api/document/' + id;
    return this.http.get(url).toPromise().catch(err => {
      console.error('GET [Documents] An error occurred', err);
    })
  }

  getDocumentRulesByDocId(id): Promise<any> {
    const url = 'api/document/more/' + id;
    return this.http.get(url).toPromise().catch(err => {
      console.error('GET [Documents] An error occurred', err);
    })
  }

  searchInDocument(keyWord): Promise<any> {
    const url = 'api/document/findRule/' + keyWord ;
    return this.http.get(url).toPromise().catch(err => {
      console.error('GET [searchInDocuments] - An error occurred', err);
    })
  }

  postFile(fileToUpload: File): Promise<any> {
    const url = 'api/document/upload';
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(url, formData).toPromise().catch(err => { 
      console.error('PostDocument - An error occured', err);
    })
  }

  searchRules(words): Promise<any> {
    const url = 'api/document/findRules';
    const wordsTable = words.split(' ') 
    const formData = {
      "words": wordsTable
    }
    return this.http.post(url, formData).toPromise().catch(err => { 
      console.error('SearchRules - An error occured', err);
    })
  }

}
