# FrontQualibri

Ce projet front est réalisé en Angular 8.

## Lancement du front

Lancer `npm install` pour installer tout les packages utilisés.

Vérifier que le serveur NodeJS est bien lancé.

Lancer simplement `npm start`

Connecter vous à l'adresse `http://localhost:4200/`. Aucun mot de passe n'est requis sur cette version de démonstration.